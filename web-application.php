
<?php  
       // require 'client-history.php';  client_history("bdkernel/web-application.php")  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:36 GMT -->
<head>
     <?php include 'layout/cssJsResource.php'; ?>
      <script type="text/javascript">
          $(document).ready(function () {
             $(".mainNav li").removeClass("active");
             $("#webApplicaitonNavId").addClass("active");
          });
      </script>

</head>

<body>

    <!--Header-->
     <?php include 'layout/header.php'; ?>
    <!-- /header -->

    <section id="bannerArea">
        <div class="bannerBlock" style="background-image: url(images/sample/slider/intro-bg.jpg); ">
            <div class="container">
                <div class="row">
                    <div class="bannnerOneFourthBlock">
                        <div class="servicesContent">
                            <h4 class="bannerTitle">WEB APPLICATION</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 



    <section class="innerStepSection">
        <div class="container" style="padding: 20px;">
            <div class="servicesContent">
                <h3 class="sectionTitle">Web Application Service</h3>
            </div>
            <div class="innerStepArea">
                <ul class="innerStepList">
                    <li >
                        <span>
                            <i class="fa icon-check" aria-hidden="true"></i>
                            <h5>PHP MySql</h5>
                             <div class=" text-justify">
                                <b>PHP</b> is in fact, one of the most popularly used languages for web development as well as web application development. With the help of PHP, you may connect and can have control over the databases, create custom coded forms as well advanced business logic and business workflow. MySQL is one of the most utilized databases with PHP.
                            </div>
                            <br/>
                            <b>Features:</b>
                            <p>As compared to other development platforms, PHP offers you a great pool of PHP developers. Extensive skills are readily available and very affordable for the businesses</p>
                            <p>PHP is open source, it is free of cost. You need not buy expensive software for it. Your website will be developed in the minimal cost.</p>
                            <p>PHP is the best web development platform for the small and medium enterprises</p>
                            <p>It works very well with different web technologies that include Javascript, CSS and HTML5</p>
                            <p>It is one of the most secured way of developing websites and web applications; as it has got a security layer to protect against viruses and threats.</p>
                        </span>
                    </li>
                    <li >
                        <span>
                            <i class="fa icon-check" aria-hidden="true"></i>
                            <h5>.Net Framework</h5>
                             <div class=" text-justify">
                                <b>.NET</b> is an integral part of many applications running on Windows and provides common functionality for those applications to run. This is primarily for on site based solution for clients who need .NET to run an application on their computer/site. For developers, the .NET Framework provides a comprehensive and consistent programming model for building applications that have visually stunning user experiences and seamless and secure communication. The .NET Framework an integral Microsoft Windows® component for building and running the next generation of applications and XML Web services. The .NET Framework delivers business value with faster time-to-market, improved system flexibility, and reduction in costs.
                            </div>
                            <br/>
                            <b>Features:</b>
                            <p>.Net brings the top level enterprise solution architecture for the businesses</p>
                            <p>Extensive use of XML, XSL, VB, SQL Server, MSMQ technologies and C#</p>
                            <p>It offers end-to-end management solution that includes server side controls design, component design, coding and then testing and final implementation</p>
                            <p>Re-engineering and migration of existing apps</p>
                            <p>Development and design of multi-level architectures for the best possible enterprise solutions</p>
                           
                        </span>
                    </li>
                   
                </ul>
            </div>
        </div>
    </section>

<section class="devProcess">
    <div class="container" style="padding: 20px">
        <div class="row-fluid">
            <div class="span2">
                <div class="clearfix">
                    <h3 class="sectionTitle">How We Do It</h3>
                </div>
                <p  class=" text-justify">This model will best suit companies which want to see the development first and then pay or to those who are on tight budget. The payment will be made on achieving agreed targets or milestones. </p>
            </div>
            <div class="span10">
                            <ul class="thumbnails">
                                <li class="span2">
                                   <div class="our-process-item "> 
                                      <i class="fa icon-lightbulb"></i>
                                      <h4>Planning and Analysis</h4>
                                      <p  class=" text-justify" style="padding: 0px">In this stage, conduct a preliminary analysis, propose alternative solutions, describe costs and benefits and submit a preliminary plan with recommendations.</p>
                                    </div>
                                </li>
                                <li class="span2">
                                       <div class="our-process-item "> 
                                          <i class="fa icon-laptop"></i>
                                          <h4>System Design</h4>
                                          <p  class=" text-justify" style="padding: 0px"> Describes desired features and operations in detail, including screen layouts, business rules, process diagrams, etc.</p>
                                      </div>
                                </li>
                                <li class="span2">
                                    <div class="our-process-item "> 
                                        <i class="fa icon-desktop"></i>
                                        <h4>Development</h4>
                                        <p  class=" text-justify" style="padding: 0px">After analysis and design of the software, the real code is written in this stage. </p>
                                    </div>
                                </li>
                                <li class="span2">
                                    <div class="our-process-item "> 
                                        <i class="fa icon-user"></i>
                                        <h4>QA and Testing</h4>
                                        <p  class=" text-justify" style="padding: 0px"> Brings all the pieces together into a special testing environment, then checks for errors, bugs and interoperability.</p>
                                    </div>
                                </li>
                                <li class="span2">
                                    <div class="our-process-item "> 
                                        <i class="fa icon-keyboard"></i>
                                        <h4>Documentation</h4>
                                        <p  class=" text-justify"  style="padding: 0px">We provide proper training and documentation. So that user can easily adopt the system .</p>
                                    </div>
                                </li>
                                <li class="span2">
                                      <div class="our-process-item "> 
                                          <i class="fa icon-bar-chart"></i>
                                          <h4>Deployment and Support</h4>
                                          <p  class=" text-justify"  style="padding: 0px">During the maintenance stage of the SDLC, the system is assessed to ensure it does not become obsolete.</p>
                                      </div>

                                </li>
                            </ul>

            </div>
        </div>
    </div>
  </section>


 <?php include 'layout/footer.php'; ?>
<!--/Footer-->


</body>

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:40 GMT -->
</html>
