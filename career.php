<?php  
       // require 'client-history.php';  client_history("bdkernel/career.php")  
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:36 GMT -->
<head>
     <?php include 'layout/cssJsResource.php'; ?>

     <style type="text/css">
       
       .innerStepList li {
            box-sizing: border-box;
            display: inline-block;
            padding: 10px;
            vertical-align: top;
            width: 100%;
        }
     </style>


      <script type="text/javascript">
          $(document).ready(function () {
             $(".mainNav li").removeClass("active");
             $("#careerNavId").addClass("active");
          });
      </script>

</head>

<body>

    <!--Header-->
     <?php include 'layout/header.php'; ?>
    <!-- /header -->

    <section id="bannerArea">
        <div class="bannerBlock" style="background-image: url(images/sample/slider/personal-website-banner.jpg); ">
            <div class="container">
                <div class="row">
                    <div class="bannnerOneFourthBlock">
                        <div class="servicesContent">
                            <h4 class="bannerTitle">CAREER</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 



    <section class="innerStepSection">
        <div class="container" style="padding: 20px;">
            <div class="servicesContent">
                <h3 class="sectionTitle">Career</h3>
            </div>
            <div class="career">
                  <div class="span2">
                    <img src="images/sample/careers-img.jpg" class="img-responsive">
                </div>
                  <div class="span10">
                  <a href="#srse">1. Sr. Web Designer</a> <img src="http://www.brotecs.com/images/new.png"><br>
                  <a href="#sqa"> 2. SQA Engineer</a> <img src="http://www.brotecs.com/images/new.png">
                   

                  <br><br><br>





                  <a class="anchor" name="srse">SQA Engineer</a>

                  <h4><a >Senior Web Designer</a></h4>
                  <p class="orange-heading">Application Open</p>
                  <p>
                  </p><p><strong>Position:</strong> Sr. Web Designer<br>
                  <br>
                  <strong>No. of Vacancies:</strong>&nbsp; 01<br>
                  <br>
                  <strong>Job  Description / Responsibility</strong></p>
                  <ul type="disc">
                        <li> PLEASE DO NOT APPLY IF YOU DON`T HAVE 4 - 5 YEARS OF EXPERIENCE IN WEB DESIGN.</li>
                        <li> Designing layout for the new websites and landing pages.</li>
                        <li> Creating intuitive interfaces and interaction layers using best practices, the latest web trends and all available tools to augment the user experience. </li>
                        <li> Ensure team output meets time and quality commitment.</li>
                        <li>Developing and maintaining brand websites</li>
                        <li>Conceptualising, creating and executing amazing design ideas. Your design solutions should impress and surprise.</li>
                        <li>Collaborate and work closely with other creatives and developers.</li>
                        <li>You will always be encouraged to take an entrepreneurial and proactive approach</li>

                  </ul>
                  <br/>
                  <p><strong>Job Nature </strong></p>
                  <p>Full-time </p>


                  <!-- <p><strong>Educational Requirements </strong></p>
                  <p>B. Sc (Hons) in Computer Science (CS) or Computer Science and Engineering (CSE) with a good academic record.</p> -->


                  <p><strong>Experience Requirements</strong></p>
                  <p>4 to 5 year(s)</p>


                  <p><strong>Job Requirements</strong></p>
                  <ul type="disc">
                        <li>Strong verbal and written communication, interpersonal skill, good work ethics.</li>
                        <li>Ability to meet deadlines and achieve specified results. </li>
                        <li>Ability to work independently and as a team under minimum supervision.</li>
                        <li>HTML 5, CSS 3, Bootstrap, JavaScript / JQuery.</li>
                        <li>Knowledge of Bootstrap framework, Responsive Web.</li>
                        <li>Knowledge of Node.js or Angular.js - added advantage.</li>
                        <li>Strong visual and graphic design skills with a good eye for detail.</li>
                        <li>Self-censor, catching and correcting your own errors.</li>
                        <li>Always looking to improve the quality of your design work</li>
                  </ul>
                  <br/>
                  <p><strong>Salary</strong><strong> Range</strong><strong>: </strong>Negotiable</p>

                  <p><strong>Other Benefits</strong></p>

                  <ul type="disc">
                        <li>Friendly Environment (Fun &amp; Work) </li>
                        <li>Lunch Allowance for Every Working Day</li>
                        <li>Two Annual Festival Bonuses</li>
                        <li>Yearly increment based on performance</li>
                        <li>General Leave and Sick Leave</li>
                        <li>Home Internet Service </li>
                  </ul>
                  <br/>
                  <p><strong>Job Location</strong>: Uttara, Dhaka</p>
                  <p><strong>Send Your CV and Photograph to:</strong> <span style="color:#ED253C">hr@bdkernel.com</span></p><p></p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <!--End-2-->







                  <a class="anchor" name="sqa">SQA Engineer</a>
                  <h4><a >SQA Engineer</a></h4>
                  <p class="orange-heading">Application Open</p>

                  <p></p><p><strong>Position:</strong> SQA Engineer<br>
                  <br>
                  <strong>No. of Vacancies:</strong>&nbsp; 02<br>
                  <br>
                  <strong>Job  Description / Responsibility</strong></p>

                  <ul type="disc">
                      <li>PLEASE DO NOT APPLY IF YOU HAVEN`T BASIC KNOWLEDGE IN SQA AND LINUX.</li>
                      <li>Role of Software Quality Analyst will test and finding bug, analyzing in details level, bug reporting and follow up the bugs.</li>
                      <li>Should have a good knowledge about SQA process.</li>
                      <li>Should Design and develop test cases, Perform testing and analysis from client point of view, develop, distribute and execute test plans so that product is delivered high accuracy.</li>
                      <li>Should understand software requirement specification with the architecture of assigned modules.</li>
                      <li>Should be able to estimate QA hour for projects, work orders etc.</li>
                      <li>Develop and document test cases, configurations and test environments.</li>
                      <li>Responsible for ensuring that the bug has been fixed as owner of the projects.</li>
                      <li>Should come up with ideas on improving ongoing process.</li>
                      <li>Should be curious to try different testing tools and promote those that fit for projects/company objectives.</li>
                      <li>Should be able to create huge number of test cases in short time with high accuracy. Also should be able to justify risk based testing.</li>
                      <li>Maintain product consistency throughout product cycle.</li>
                      <li>Perform R&amp;D to identify the scope of test ideas that may scale up the output level.</li>
                  </ul>

                  <p><strong>Job Nature </strong></p>
                  <p>Full-time </p>


                  <p><strong>Educational Requirements </strong></p>
                  <p>B. Sc (Hons) in Computer Science (CS) or Computer Science and Engineering (CSE) with a good academic record.</p>


                  <p><strong>Experience Requirements</strong></p>
                  <p>0 to 1 year</p>

                  <p><strong>Additional Job Requirements</strong></p>
                  <ul type="disc">
                      <li>Need to be able to understand and convey both viewpoints of the Developers as well the Clients, performing the role of a mediator.</li>
                      <li>Must be familiar with the entire SDLC in order to be effective.</li>
                      <li>Must have working experience in Linux platform.</li>
                      <li>Should have strong communication skills.</li>
                      <li>Having knowledge of QA automation tools is a strong plus.</li>
                      <li>Should be able to work in a team environment.</li>
                      <li>Must be proactive, hard worker, energetic &amp; determined to meet deadlines.</li>
                      <li>Familiar with different programming languages and logic.</li>
                  </ul>

                  <p><strong>Salary</strong><strong> Range</strong><strong>: </strong>Negotiable</p>

                  <p><strong>Other Benefits</strong></p>

                  <ul type="disc">
                      <li>Friendly Environment (Fun &amp; Work) </li>
                        <li>Lunch Allowance for Every Working Day</li>
                        <li>Two Annual Festival Bonuses</li>
                        <li>Yearly increment based on performance</li>
                        <li>General Leave and Sick Leave</li>
                        <li>Home Internet Service </li>
                  </ul>

                  <p><strong>Job Location</strong>: Uttara, Dhaka</p>

                  <p><strong>Send Your CV and Photograph to:</strong> <span style="color:#ED253C">hr@bdkernel.com</span></p><p></p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <!--End-SQA-->



              </div>
              
            </div>
        </div>
    </section>


 <?php include 'layout/footer.php'; ?>
<!--/Footer-->


</body>

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:40 GMT -->
</html>
