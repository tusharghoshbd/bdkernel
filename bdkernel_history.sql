-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 07, 2017 at 06:53 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bdkernel_history`
--

-- --------------------------------------------------------

--
-- Table structure for table `client_visit_history`
--

CREATE TABLE `client_visit_history` (
  `id` int(10) NOT NULL,
  `client_name` varchar(256) NOT NULL,
  `country` varchar(50) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `continent` varchar(50) NOT NULL,
  `continent_code` varchar(50) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_visit_history`
--

INSERT INTO `client_visit_history` (`id`, `client_name`, `country`, `country_code`, `state`, `city`, `continent`, `continent_code`, `date_time`) VALUES
(2, 'bdkernel', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '0000-00-00 00:00:00'),
(3, 'bdkernel', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '0000-00-00 00:00:00'),
(4, 'bdkernel', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '0000-00-00 00:00:00'),
(5, 'larrybartels', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '0000-00-00 00:00:00'),
(6, 'bdkernel', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '0000-00-00 00:00:00'),
(7, 'bdkernel', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '2017-05-07 12:37:53'),
(8, 'larrybartels', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '2017-05-07 12:40:59'),
(9, 'bdkernel', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '2017-05-07 12:46:44'),
(10, 'bdkernel', 'Bangladesh', 'BD', 'Dhaka', 'Dhaka', 'Asia', 'AS', '2017-05-07 12:50:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_visit_history`
--
ALTER TABLE `client_visit_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client_visit_history`
--
ALTER TABLE `client_visit_history`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
