

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title> BDKERNEL | Your Website Knocking Your Door</title>

<meta name="description" content="Making your personal website where you can create web pages about anything you like and get it all out there for everyone to see">
<meta name="keywords" content="Personal Website, BDKERNEL, Website design, web application, Own Website, uniquely represents yourself to online">

<meta name="viewport" content="width=device-width">


<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery.bxslider.min.css">

<link rel="stylesheet" href="css/sl-slide.css">
<link rel="stylesheet" href="css/main.css">
<!-- AIzaSyBe8KsXanQqwbuCIbbPoss-86eRele8s2Q -->



<script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>


    <link rel="shortcut icon" href="images/sample/bdkernel_logo.png">


<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<!-- <script src = "js/google.map.js"></script> -->
 
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe8KsXanQqwbuCIbbPoss-86eRele8s2Q&callback=initMap"
  type="text/javascript"></script> 

<!-- Required javascript files for Slider -->
<script src="js/jquery.ba-cond.min.js"></script>
<!-- <script src="js/owl.carousel.min.js"></script> -->
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/main.js"></script>
<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>
<!-- /Required javascript files for Slider -->

 <script type="text/javascript">
        $(document).ready(function () {
            /* Sticky Menu Bar */
            $(window).scroll(function () {
                if ( $(this).scrollTop() > 38 ) {
                    $(".bottomHeader").addClass("fixed");
                }
                else {
                     $(".bottomHeader").removeClass("fixed");
                }
            });

            $('.nav-icon').click(function(){
                $(this).next().slideToggle();
            });


             setTimeout(function() { initMap(); console.log("test")}, 200);

        //initMap() ;


        setInterval(blinker, 1000);           
        });

        function blinker() {
          $('.blinking').fadeOut(500);
          $('.blinking').fadeIn(500);
        }

         function initMap() {
               var myCenter = new google.maps.LatLng(23.8306, 90.4174);
                var mapCanvas = document.getElementById("map");
                var mapOptions = {center: myCenter, zoom: 15,
                        zoomControl: false,
                        scaleControl: false,
                        scrollwheel: false};
                var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({position:myCenter});
                marker.setMap(map);
      }




    </script>