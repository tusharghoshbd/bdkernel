
<!-- <section class="bottom">
    <div class="container"  style="padding-left: 20px; padding-right: 20px">
        <div class="row-fluid">
            <div class="span3">
                <h4 class="title-block">ADDRESS</h4>
                <ul class="unstyled address">
                    <li>
                        <i class="icon-home"></i><strong>Address:</strong> Baraibogh Enayetnagar<br>Fatullah, Narayangonj-1400<br>Bangladesh
                    </li>
                    <li>
                        <i class="icon-envelope"></i>
                        <strong>Email: </strong> timeknit@gmail.com
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <strong>Website:</strong> www.timeknit.net
                    </li>
                    <li>
                        <i class="icon-phone"></i>
                        <strong>Cell No:</strong> +88 01612 151 215 
                    </li>
					<li>
                        <i class="icon-phone"></i>
                        <strong>Cell No:</strong> +88 01722 091 707 
                    </li>
                </ul>
            </div>
            <div id="tweets" class="span3">
                <h4 class="title-block">NAVIGATION</h4>
                <div>
                    <ul class="arrow">
                     <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Personal Website</a></li>
                        <li><a href="#">Website Design</a></li>

                        <li><a href="#">Web Application</a></li>
                        <li><a href="#">Ecommerce</a></li>
                        </ul>
                </div>  
            </div>

                <div id="tweets" class="span6">
                 <div id = "map" style = "width:570px; height:580px;"></div>
            </div>

        </div>

    </div>
</div> 

</section> -->


<section class="bottom">
        <div class="container">
            <div class="footerBlock">
                 <div class="span3">
                <h4 class="title-block">ADDRESS</h4>
                <ul class="unstyled address">
                    <li>
                        <i class="icon-home"></i><strong>Address:</strong> House:21/B (1<sup>st</sup> floor), <br/>Nikunja-02, Khilkhet<br> Dhaka-1229<br>Bangladesh
                    </li>
                    <li>
                        <i class="icon-envelope"></i>
                        <strong>Email: </strong> info@bdkernel.com
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <strong>Website:</strong> www.bdkernel.com
                    </li>
                    <!-- <li>
                        <i class="icon-phone"></i>
                        <strong>Cell No:</strong> +88 01612 151 215 
                    </li>
                    <li>
                        <i class="icon-phone"></i>
                        <strong>Cell No:</strong> +88 01722 091 707 
                    </li> -->
                </ul>
            </div>
            <!-- <div id="tweets" class="span3">
                <h4 class="title-block">NAVIGATION</h4>
                <div>
                    <ul class="arrow">
                     <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Personal Website</a></li>
                        <li><a href="#">Website Design</a></li>

                        <li><a href="#">Web Application</a></li>
                        <li><a href="#">Ecommerce</a></li>
                        </ul>
                </div>  
            </div> -->

            </div>
        </div>
         <div id = "map" ></div>
  </section>

<footer class="footer">
    <div class="container"  style="padding-left: 20px; padding-right: 20px">
        <div class="row-fluid">
            <div class="span5 cp">
                &copy; 2017 <a target="_blank" href="https://www.facebook.com/OrigamiCloudSolutions/?fref=ts" title="Free Twitter Bootstrap WordPress Themes and HTML templates">BDKERNEL</a>. All Rights Reserved.
            </div>
            <!--/Copyright-->

            <div class="span6">
                <ul class="social pull-right">
                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                    <li><a href="#"><i class="icon-google-plus"></i></a></li>                       
                    <li><a href="#"><i class="icon-youtube"></i></a></li>
                                      
                </ul>
            </div>

            <div class="span1">
                <a id="gototop" class="gototop pull-right" href="#"><i class="icon-angle-up"></i></a>
            </div>
            <!--/Goto Top-->
        </div>
    </div>
</footer>
 
