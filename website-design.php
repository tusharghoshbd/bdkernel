
<?php  
       // require 'client-history.php';  client_history("bdkernel/website-design.php")  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:36 GMT -->
<head>
     <?php include 'layout/cssJsResource.php'; ?>
      <script type="text/javascript">
          $(document).ready(function () {
             $(".mainNav li").removeClass("active");
             $("#websiteDesignNavId").addClass("active");
          });
      </script>

</head>

<body>

    <!--Header-->
     <?php include 'layout/header.php'; ?>
    <!-- /header -->

    <!-- <section class="title">
        <div class="container">
            <div class="row-fluid">
                <div class="span6">
                    <h1>Website Design</h1>
                </div>
                <div class="span6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="index-2.html">Home</a> <span class="divider">/</span></li>
                        <li class="active">About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </section> -->
    <!-- / .title -->  






    <section id="bannerArea">
        <div class="bannerBlock" style="background-image: url(images/sample/slider/inner-web-design.jpg); ">
            <div class="container">
                <div class="row">
                    <div class="bannnerOneFourthBlock">
                        <div class="servicesContent">
                            <h4 class="bannerTitle">WEBSITE DESIGN</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 



    <section class="innerStepSection">
        <div class="container" style="padding: 20px;">
            <div class="servicesContent">
                <h3 class="sectionTitle">Website Design Service</h3>
            </div>
            <div class="innerStepArea">
                <ul class="innerStepList">
                    <li >
                        <span>
                            <i class="fa icon-check" aria-hidden="true"></i>
                            <h5>Responsive Website Design</h5>
                             <div class=" text-justify">
                                Providing the best experience to your audience means catering to a variety of devices. The most cutting edge and cost effective way to accomplish this is through Responsive Web Design. Responsive Web Design has developed over the past few years to become a device agnostic approach. From a mobile browser on an iPhone all the way up to a HDTV, responsive websites are able to adjust their appearance based on screen dimensions.
                            </div>
                            <br/>
                            <b>Features:</b>
                            <p>You send/we collect your requirements and business details.</p>
                            <p>Supporting the Multi-Device User </p>
                            <p>Improved Search Engine Rankings</p>
                            <p>Future Scalability</p>
                            <p>User Friendly website</p>
                            <p>Search Engine Optimization (SEO)</p>
                        </span>
                    </li>
                    <li >
                        <span>
                            <i class="fa icon-check" aria-hidden="true"></i>
                            <h5>CMS Website Design</h5>
                             <div class=" text-justify">
                                Our CMS like Wordpres, Joomla,  was designed to support for an Enterprise-wide web application deployment. This includes a friendly user interface, a document and records management system and a component management system. Users who understand office type applications can store, control, organize, revise, version and publish content over the web without programming or technical knowledge. It has Full Text Search functionality making it easy to find documents form web achieves.
                            </div>
                            <br/>
                            <b>Features:</b>
                            <p>You send/we collect your requirements and business details.</p>
                            <p>Supporting the Multi-Device User </p>
                            <p>Improved Search Engine Rankings</p>
                            <p>Future Scalability</p>
                            <p>User Friendly website</p>
                            <p>Search Engine Optimization (SEO)</p>
                        </span>
                    </li>
                   
                </ul>
            </div>
        </div>
    </section>
<!--

<section class="snapShot">
  <div class="container" style="padding: 20px">
<h2 class="animated fadeInRight" data-fx="fadeInRight" style="visibility: visible;">Company Snapshot</h2>
<hr>
<div class="row">
<div class="colume">
<div class="number-counter ico01" data-perc="94"><span class="number-count highlight">94</span></div>
<h5 class="number-details">Team Size</h5>
</div>
<div class="colume">
<div class="number-counter ico02" data-perc="766"><span class="number-count highlight">766</span></div>
<h5 class="number-details">Sites Launched</h5>
</div>
<div class="colume">
<div class="number-counter ico03" data-perc="59"><span class="number-count highlight">59</span></div>
<h5 class="number-details">Apps Launched</h5>
</div>
<div class="colume">
<div class="number-counter ico04" data-perc="153"><span class="number-count highlight">153</span></div>
<h5 class="number-details">Partnerships</h5>
</div>
<div class="colume">
<div class="number-counter ico05" data-perc="15"><span class="number-count highlight">15</span></div>
<h5 class="number-details">Awards</h5>
</div>
</div>
</div>
</section>
-->
<section class="devProcess">
    <div class="container" style="padding: 20px">
        <div class="row-fluid">
            <div class="span2">
                <div class="clearfix">
                    <h3 class="sectionTitle">How We Do It</h3>
                </div>
                <p  class=" text-justify">This model will best suit companies which want to see the development first and then pay or to those who are on tight budget. The payment will be made on achieving agreed targets or milestones. </p>
            </div>
            <div class="span10">
                            <ul class="thumbnails">
                                <li class="span3">
                                   <div class="our-process-item "> 
                                      <div> <i class="fa icon-lightbulb"></i></div>
                                     
                                      <h4>STRATEGY</h4>
                                      <p  class=" text-justify">Define objective brand analysis, keyword research and positioning strategy.</p>
                                    </div>
                                </li>
                                <li class="span3">
                                       <div class="our-process-item "> 
                                          <i class="fa icon-laptop"></i>
                                          <h4>DESIGN</h4>
                                          <p  class=" text-justify">We settle on some initial design drafts for your website and choose one concept.</p>
                                      </div>
                                </li>
                                <li class="span3">
                                    <div class="our-process-item "> 
                                        <i class="fa icon-keyboard"></i>
                                        <h4>DEVELOPMENT</h4>
                                        <p  class=" text-justify">To make the content, information architecture, visual design all work and function together.</p>
                                    </div>
                                </li>
                                <li class="span3">
                                      <div class="our-process-item "> 
                                          <i class="fa icon-bar-chart"></i>
                                          <h4>DEPLOYMENT</h4>
                                          <p  class=" text-justify">Our team of experts are always available for any updates you may need.</p>
                                      </div>

                                </li>
                            </ul>

            </div>
        </div>
    </div>
</section>



 <?php include 'layout/footer.php'; ?>
<!--/Footer-->


</body>

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:40 GMT -->
</html>
