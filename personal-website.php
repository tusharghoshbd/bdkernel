<?php  
       // require 'client-history.php';  client_history("bdkernel/personal-website.php")  
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:36 GMT -->
<head>
     <?php include 'layout/cssJsResource.php'; ?>

     <script type="text/javascript">
        $(document).ready(function () {
           $(".mainNav li").removeClass("active");
           $("#personalWebsiteNavId").addClass("active");
        });
     </script>
</head>

<body>

    <!--Header-->
     <?php include 'layout/header.php'; ?>
    <!-- /header -->


    <section id="bannerArea">
        <div class="bannerBlock" style="background-image: url(images/sample/slider/personal-website-banner.jpg); ">
            <div class="container">
                <div class="row">
                    <div class="bannnerOneFourthBlock">
                        <div class="servicesContent">
                            <h4 class="bannerTitle">PERSONAL WEBSITE</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<section class="innerStepSection">
        <div class="container" style="padding: 20px;">
            <div class="servicesContent">
                <h3 class="sectionTitle">Personal Website</h3>
            </div>
            <div class="row-fluid" style="padding-top: 10px;">
                <div>
                    <p class=" text-justify"> A personal website is a group of Web pages that basically created by an individual to contain content of a personal nature rather than content pertaining to a company, organization or institution. Personal page are primarily used for infromative or entertainmetn purpose but can also be used for persoanl career marketing, social networking with other people with shared interests, or as a space for persoanl expression.  It must exhibit content that tells your readers about your profession, thoughts, ideas, interests, hobbies, family, friends, feelings, or something you feel strongly.</p>

                    <p class=" text-justify">With the rise of social networks, a personal website is no longer the only place online where you can express yourself. However, having a personal site still offers significant benefits. For example, With a website of your own you will have the freedom to express yourself in a manner not possible with any social network And if you want to create an online portfolio or a resume, there is no better approach than a personal website.</p>
                    <br> 

                </div>
            </div>
            <div class="innerStepArea">
                <div class="gap"></div>
                <div class="center">
                    <h3  style="color:#34495E" class="uppercase">Why Personal Website</h3>
                </div>  
                
                <ul class="innerStepList">
                    <li >
                        <span>
                            <div class="grid-2-1" id="filterAbout">
                                <div class="grid-3 clearfix">
                                    <div class="discover-Block">
                                        <div class="ico"><a href="javascript:">&nbsp;</a></div>
                                        <div class="description">
                                            <h4 class="uppercase">Job seeker</h4>                                            
                                            <div class=" text-justify">
                                            A personal website gives you creative freedom to express your personality in ways that are not be possible through your resume. Everything from the bio paragraph you write to the design options you choose for your website says something about you, and gives recruiters more chances to decide if they want to bring you in for an interview.</div><br/>

                                            <div class=" text-justify">According to <a href="http://www.workfolio.com/" target="_blank" style="color:#005580">Workfolio</a>, a newly launched company that develops applications for professional visibility, 56% of all hiring managers are more impressed by a candidate’s personal website than any other personal branding tool—however, only 7% of job seekers actually have a personal website.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" clearfix">
                                    <div class="design-Block">
                                        <div class="ico"><a href="javascript:">&nbsp;</a></div>
                                        <div class="description">
                                            <h4 class="uppercase">Tell your Story</h4>
                                            <div class=" text-justify">Unlike a traditional resume, your website gives future employers a better look into your real personality and achievements. With hiring managers receiving hundreds of resumes, your personal website is a sure fire way to stand out in the crowd. It helps more effectively breakout startup experience vs “traditional” career path, something that is very challenging to do on a resume.</div><br/>

                                            <div class=" text-justify">The same thing applies to having your own custom e-mail connected to your domain and website - it looks much more professional to use your custom e-mail.</div>
                                        </div>
                                    </div>
                                </div>
                            </span>
                    </li>
                    <li >
                        <span>
                                
                            <div class="grid-2-1" id="filterAbout">
                                <div class="grid-3 clearfix">
                                    <div class="development-Block">
                                        <div class="ico"><a href="javascript:">&nbsp;</a></div>
                                        <div class="description">
                                            <h4 class="uppercase">Build and Control Your Brand</h4>
                                            <div class=" text-justify">We are all CEO’s of our personal brand. Companies know the importance of creating a brand message and actively communicating that message to consumers. A personal website is a great way to communicate your brand to potential employers, clients and others who may be “Googling” you.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-3 clearfix">
                                    <div class="marketing-Block">
                                        <div class="ico"><a href="javascript:">&nbsp;</a></div>
                                        <div class="description">
                                            <h4 class="uppercase">Security</h4>
                                            <div class=" text-justify">There are many websites or platforms where you are representing yourself now. You have a opportunity to represent yourself in your own website. You can manage your website your way. It's more secure and more customizable. 
<br/>
                                            <br/>A domain name costs less than a beer nowadays. That to me is a cheap hedge against somebody else owning your domain name and directing it to who the hell knows where. Even if you don’t plan on building out your site just yet, do yourself a favor and purchase your unique domain name.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" clearfix">
                                    <div class="design-Block">
                                        <div class="ico"><a href="javascript:">&nbsp;</a></div>
                                        <div class="description">
                                            <h4 class="uppercase">Invest in Yourself</h4>
                                            <div class=" text-justify">Some people may shy away from creating their own page because they fear the cost of website design. The good news is that, especially considering the plethora of benefits that a personal site will bestow upon you and your business, it’s worth the price tag. It helps you save on the cost of paper products and other supplies you’d need to keep clients up to date in other, more traditional ways.</div>
                                        </div>
                                    </div>
                                </div>
                               
                              </div>
                           
                           
                        </span>
                    </li>
                   
                </ul>
            </div>
        </div>
    </section>





<!--Footer-->
 <?php include 'layout/footer.php'; ?>
<!--/Footer-->

<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/main.js"></script>

</body>

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:40 GMT -->
</html>
