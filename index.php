<?php  
       // require 'client-history.php';  client_history("bdkernel/index.php")  
?>



<!DOCTYPE html>

<head>
    <?php include 'layout/cssJsResource.php';?>

    <script type="text/javascript">
        $(document).ready(function () {
           $(".mainNav li").removeClass("active");
           $("#homeNavId").addClass("active");
           // setInterval( "slideSwitch()", 3000 );
           $('#tbsId').bxSlider({
                auto: true,
                autoControls: false,
                controls: false,
                pager :false,
                pause: 3000,
                speed:300,
                autoHover: true

           });
            $('#prosmartId').bxSlider({
                auto: true,
                autoControls: false,
                controls: false,
                pager :false,
                pause: 3000,
                speed:300,
                autoHover: true

           });
            $('#softepwId').bxSlider({
                auto: true,
                autoControls: false,
                controls: false,
                pager :false,
                pause: 3000,
                speed:300,
                autoHover: true

           });
            $('#tiktokId').bxSlider({
                auto: true,
                autoControls: false,
                controls: false,
                pager :false,
                pause: 3000,
                speed:300,
                autoHover: true

           });

            $('#myCarousel').carousel({
                 interval: 4500
             })

        });
     </script>


</head>

<body>

    <!--Header-->
    <?php include 'layout/header.php';

?>
    <!-- /header -->

  <section id="slide-show">
     <div id="slider" class="sl-slider-wrapper">

        <!--Slider Items-->    
        <div class="sl-slider">
            <!--Slider Item1-->
            <div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                        <img class="slide-image-shape" src="images/sample/slider/knocking_door.png" alt=""  />
              
            </div>
            <!--/Slider Item1-->

            <!--Slider Item2-->
            <div class="sl-slide item2" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
                        <img class="slide-image-shape" src="images/sample/slider/unique_represent.jpg" alt="" />
            </div>
            <!--Slider Item2-->

            <!--Slider Item3-->
            <div class="sl-slide item3" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
                    <img class="slide-image-shape" src="images/sample/slider/designanddevelopment.jpg" alt=""  />
        </div>
        <!--Slider Item3-->
   <!--Slider Item4-->
           <div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                        <img class="slide-image-shape" src="images/sample/slider/bdk_solution.jpg" alt=""  />
            </div>
        <!--Slider Item4-->
        
    </div>
    <!--/Slider Items-->

    <!--Slider Next Prev button-->
    <nav id="nav-arrows" class="nav-arrows">
        <span class="nav-arrow-prev"><i class="icon-angle-left" ></i></span>
        <span class="nav-arrow-next"><i class="icon-angle-right"></i></span> 
    </nav>
    <!--/Slider Next Prev button-->

</div>
<!-- /slider-wrapper -->           
</section>



<!--Services-->
<section id="services">
    <div class="container">
        <div class="center gap">
            <h3>BDKERNEL SOLUTION</h3>
            <hr/>
        </div>

        <div class="row-fluid">
            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-globe icon-medium"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="personal-website.php">Personal Website</a></h4>
                        <p class=" text-justify">
                        A personal website is a group of Web pages that basically contains personal things. It must exhibit content that tells your readers about your profession, thoughts, ideas, interests, hobbies, family, friends, feelings, or something you feel strongly.
                        </p>
                    </div>
                </div>
            </div>            

            <div class="span4">
                <div class="media">
                    <div class="pull-left"><i class="icon-laptop icon-medium"></i>
                    </div>
                    <div class="media-body">
                        <h4 class=" media-heading "><a href="website-design.php">Website Design</a></h4>
                        <p class=" text-justify">
                        BDKERNEL team is very much experienced & creative in the field of website design. We develop mobile friendly responsive, static, ecommerce website, and CMS based web application that meets the specific business needs.                       
                        </p>
                    </div>
                </div>
            </div>            


            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-desktop icon-medium icon-rounded"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="web-application.php">Web Application</a></h4>
                        <p class=" text-justify">
                        At BDKERNEL, a single project manager will coordinate your development project and ensure your goals are met on time and on budget. Whether your web development project is large or small, it has the experience and the expertise to get you there.

                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="gap"></div>

        <div class="row-fluid">
            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-shopping-cart icon-medium"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Ecommerce Solution</h4>
                        <p class=" text-justify">
                        Ecommerce has revolutionized in the retail industry. BDKERNEL take care of all e-Commerce needs of our clients – from setting up online stores and creating product catalogues, databases, through to secure transaction, order tracking, discount functions, currency conversions, and tax/ shipping calculations.
                        </p>
                    </div>
                </div>
            </div>            

            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-tablet icon-medium"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Mobile Apps</h4>
                        <p class=" text-justify">In a world, more and more people are spending their time online on a mobile device. So a mobile app for your business is crucial. BDKERNEL offers iOS, Android app design and development. Mobile app team are under development. 
                        </p>
                    </div>
                </div>
            </div>            

            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-comment icon-medium"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Online Support</h4>
                        <p class=" text-justify">When you run software in your industries that time you may need to IT support.  It’s natural and it happens especially if you are not very familiar with the technology. We provide affordable IT support package that suit to you.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


<section id="clients" class="main">
    <div class="container" style="padding-top: 30px; padding-bottom:30px">
        <div class="row-fluid">
            <div class="span2">
                <div class="clearfix">
                    <h3 class="pull-left">Key Concepts :</h3>
                    <!-- <div class="pull-right">
                        <a class="prev" href="#myCarousel" data-slide="prev">
                            <i class="icon-angle-left icon-large" style="color: #ED253C"></i>
                            </a>
                        <a class="next" href="#myCarousel" data-slide="next">
                            <i class="icon-angle-right icon-large" style="color: #ED253C"></i>
                        </a>
                    </div> -->
                </div>
                <p class=" text-justify">Making your personal website where you can create web pages about anything you like and get it all out there for everyone to see.</p>
            </div>
            <div class="span10">
                <div id="myCarousel" class="carousel slide clients">
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                    <div class="active item">
                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <li class="span3">
                                    <a href="#"><img src="images/sample/clients/we_are_different.png"></a>
                                </li>
                                <li class="span9">
                                    
                                    <span class="keyconcept">
                                        <p class=" text-justify"><b> 
                                        We are coming to you to provide your own website. 
                                         <!-- Because we are coming to provide you of your own website. --> </b>
                                         </p>
                                        <p class=" text-justify"><b>
                                       <!--  Traditionally, software firms are doing web site that is large and long term project. we make personal website that needs everyone to promote  your identity uniquely in online. -->
                                           We are providing your own website which gives you creative freedom to express your personality and promote yourself.   

                                       </b>
                                         </p>
                                        <p class=" text-justify"><b>


                                        There are many websites or platforms where you are representing yourself now. Here we are giving you the opportunity to represent yourself in your own website. We are being proud helping you to be owner of your own website  that will be named after you.


                                       <!--  But you are making identity of yours that actually created under another. So You are not unique.  we provide the opportunity to make your personal website just saying yes.  Here you will get unique domain name.  --></b>
                                         </p>
                                        <p class=" text-justify">
                                        <b>
                                            We realize  and give importance of the strength of quality.
                                        </b>
                                         </p>
                                    </span>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <!-- 
                    <div class="item">
                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <li class="span3"><a href="#"><img src="images/sample/clients/client4.png"></a></li>
                                 <li class="span9">
                                   <h5> <p class=" text-justify">Traditionally, software firms are doing web site that is large and long term project. we make personal website that needs everyone to promote  your identity uniquely in online.
                                    </p></h5>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="item">
                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <li class="span3"><a href="#"><img src="images/sample/clients/client3.png"></a></li>
                                 <li class="span9">
                                    <h5><p class=" text-justify">There are many website or platform where you can represent  yourself. But you are making identity of yours that actually created under another. So You are not unique.  we provide the opportunity to make your personal website just saying yes.  Here you will get unique domain name.  </p></h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                -->

            </div>
        </div>
    </div>
</div>
</section>




<section id="recent-works" >
    <div class="container" style="padding-top: 30px; padding-bottom: 30px">
        <div class="center">
            <h3>RECENT WORK AND TEMPLATE</h3>
            <p class="lead">Look at some of the recent projects and templates that we have completed for our valuble clients</p>
        </div>  
        <div class="gap"></div>
        <ul class="gallery col-4">
            <!--Item 1-->
            <li class="span3">
                <div class="preview" >
                    <a target="_blank" href="http://103.254.167.133:8080/tbs/index.php">
                         <ul class="bxslider" id="tbsId">
                               <li><img src="images/portfolio/full/tbs_home_page.jpg" /></li> 
                             <!--  <li><img src="images/portfolio/full/tbs_bar_graph.jpg" /></li>
                              <li><img src="images/portfolio/full/tbs_general_stock.jpg" /></li> -->
                              <li><img src="images/portfolio/full/tbs_line_graph.jpg" /></li>
                        </ul>
                     </a>                   
                </div>
                <div class="desc">
                        <h3>
                             <a target="_blank" href="http://103.254.167.133:8080/tbs/index.php">
                            TBS Portal </a>
                        </h3>
                        <p class=" text-justify">
                            TBS(Textile Business Solution) is a web application where some module of Badsha Textile ERP (previously it implemented with oracle forms) like employee attandence, general store, yarn export and import, different kind of report has been implemented.
                        </p>
                         <h5>Development Tools: </h5>
                         <p class=" text-justify"><b>PHP, Oracle, Javascript, HTML, Ajax, JQuery</b></p>
                </div>
                <div id="modal-4" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
                    
                </div>                 
            </li>
            <!--/Item 1--> 

            <!--Item 2-->
            <li class="span3">
                <div class="preview">
                  <!--   <img alt=" " src="images/portfolio/full/prosmart.jpg">
                    <div class="overlay">
                    </div> -->

                     <a target="_blank" href="http://prosmart.bdkernel.com">
                         <ul class="bxslider" id="prosmartId">
                              <li><img src="images/portfolio/full/prosmart.jpg" /></li>

                        </ul>
                     </a>  
                   
                </div>
                 <div class="desc">
                        <h3>
                            <a target="_blank" href="http://prosmart.bdkernel.com">
                            ProSMART </a>
                        </h3>
                        <p class=" text-justify">
                            ProSMART is a html template for professors. Many Professor need to maintanin their information in online. Through this template, a professor can easily exhibits his career, working activities, collboration, etc. 
                        </p>
                         <h5>Development Tools: </h5>
                         <p class=" text-justify"><b>PHP, Bootstrap, HTML, Ajax, JQuery, JavaScript</b></p>
                </div>
                <div id="modal-1" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
                    
                </div>                 
            </li>
            <!--/Item 2-->

            <!--Item 3-->
            <li class="span3">
                <div class="preview">
                    <a target="_blank" href="http://softepw.bdkernel.com">
                         <ul class="bxslider" id="softepwId">
                              <li><img src="images/portfolio/full/softepw.jpg" /></li>
                        </ul>
                     </a> 
                   
                </div>
                 <div class="desc">
                        <h3>
                            <a target="_blank" href="http://softepw.bdkernel.com">
                            SoftEPW</a>
                            
                        </h3>
                        <p class=" text-justify">
                            SoftEPW is very popular theme for software engineering. Within one page, a software engineer can exebits his career, education and current project that he finished. He can submit his website link instead of CV. 
                        </p>
                         <h5>Development Tools: </h5>
                         <p class=" text-justify"><b>PHP, Bootstrap, HTML, Ajax, JQuery, JavaScript</b></p>
                </div>
                <div id="modal-3" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
                    
                </div>                 
            </li>
            <!--/Item 3--> 

            <!--Item 4-->
            <li class="span3">
                <div class="preview">

                    <a target="_blank" href="http://tiktok.bdkernel.com">
                         <ul class="bxslider" id="tiktokId">
                              <li><img src="images/portfolio/full/tiktok.jpg" /></li>
                        </ul>
                     </a>
                   <!--  <img alt=" " src="images/portfolio/full/tiktok.jpg">
                    <div class="overlay">
                    </div> -->
                    
                </div>
                 <div class="desc">
                        <h3>
                            <a target="_blank" href="http://tiktok.bdkernel.com">
                            Tik Tok</a>
                            
                        </h3>
                        <p class="text-justify">
                            Tik Tok is personal website template to express their education, profession, and services in online. Here is a hire button where any enployer can you by clickin it.
                        </p>
                         <h5>Development Tools: </h5>
                         <p class=" text-justify"><b>Ruby on Rails, Mysql, MongoDB, HTML, Ajax, JQuery, JavaScript</b></p>
                </div>
                <div id="modal-4" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></a>
                    
                </div>                 
            </li>
            <!--/Item 4-->               

        </ul>
    </div>

</section>






<!--Footer-->
 <?php include 'layout/footer.php';

?>
<!--/Footer-->


<!-- SL Slider -->
<script type="text/javascript"> 
$(function() {
    var Page = (function() {

        var $navArrows = $( '#nav-arrows' ),
        slitslider = $( '#slider' ).slitslider( {
            autoplay : true
        } ),

        init = function() {
            initEvents();
        },
        initEvents = function() {
            $navArrows.children( ':last' ).on( 'click', function() {
                slitslider.next();
                return false;
            });

            $navArrows.children( ':first' ).on( 'click', function() {
                slitslider.previous();
                return false;
            });
        };

        return { init : init };

    })();

    Page.init();


     
});
</script>
<!-- /SL Slider -->
</body>

<!-- Mirrored from timeknit.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:26 GMT -->
</html>