
<?php  
       // require 'client-history.php';  client_history("bdkernel/about-us.php")  
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:36 GMT -->
<head>
     <?php include 'layout/cssJsResource.php'; ?>

     <script type="text/javascript">
        $(document).ready(function () {
           $(".mainNav li").removeClass("active");
           $("#aboutUsNavId").addClass("active");
        });
     </script>
</head>

<body>

    <!--Header-->
     <?php include 'layout/header.php'; ?>
    <!-- /header -->


    <section id="bannerArea">
        <div class="bannerBlock" style="background-image: url(images/sample/slider/Media-Search.jpg); ">
            <div class="container">
                <div class="row">
                    <div class="bannnerOneFourthBlock">
                        <div class="servicesContent">
                            <h4 class="bannerTitle">ABOUT US</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   


<section class="innerStepSection">
        <div class="container" style="padding: 20px;">
            <div class="servicesContent">
                <h3 class="sectionTitle">About Us</h3>
            </div>
            <div class="row-fluid" style="padding-top: 10px;">
                <div>
                    <p class=" text-justify"> At Bdkernel, we understand the reality that, personal website of your is the key way to promote yourself  around the world through internet. It will also give you a chance to showcase your skills and demonstrate your creativity. We design your website not as simple website but much more than that. </p>

                    <p class=" text-justify">BDKERNEL is full-service Web design, Web Development, Software Development company offering personal website, business website and application around the world. We specialize in web development, e-commerce, mobile development, custom website design-redesign, domain-hosting and more. 
                    </p>
                    <br/><br/>
                    <img src="images/sample/what-we-donew-banner.png" />
                    <br/> <br/>


                </div>
            </div>
            <div class="innerStepArea" style="width: 100%">
                <ul class="innerStepList">
                    <li >
                        <span>
                            <i class="fa icon-check" aria-hidden="true"></i>
                            <h5>Our Missions</h5>
                            <p>Ensure you the best Quality.</p>
                            <p> Cost-effective Services within Client Budgets.</p>
                            <p>Complete Customer Satisfaction</p>
                           
                        </span>
                    </li>
                    <li >
                        <span>
                            <i class="fa icon-check" aria-hidden="true"></i>
                            <h5>Our Vission</h5>  
                            <p>Making everyone  presence to online with unique identity.</p>
                            <p>not only develop products but also develop relationships.</p>
                            <p>To be the leader in making personal website in the world.</p>
                        </span>
                    </li>
                    
                   
                </ul>
            </div>
        </div>
    </section>



<section id="new-concept" >
    <div class="container" style="padding-top: 30px; padding-bottom: 30px">
        <div class="center">
            <h3  style="color:#34495E">WHY CHOOSE US</h3>
             <p class="lead">


We cultivate strong Customer relationships and manage capacity to ensure that each Customer receives maximum focus.
</p>
        </div>  
        <div class="gap"> <br/></div>
        <div class="row-fluid">
            <div>
                <ul class="thumbnails">
                                <li class="span4">
                                   <div class="our-process-item "> 
                                      <div> <i class="fa icon-leaf"></i></div>
                                     
                                      <h4>Commitment to Quality</h4>
                                      <p  class=" text-justify">Creating value through excellence – In innovation, quality and people.</p>
                                    </div>
                                </li>
                                <li class="span4">
                                       <div class="our-process-item "> 
                                          <i class="fa icon-laptop"></i>
                                          <h4>Customer Focus</h4>
                                          <p  class=" text-justify">Always give people more than what they expect to get. Every great business is built on friendship.</p>
                                      </div>
                                </li>
                                <li class="span4">
                                    <div class="our-process-item "> 
                                        <i class="fa icon-keyboard"></i>
                                        <h4>Competitive Price</h4>
                                        <p  class=" text-justify">Quality is unsurpassed while the prices are affordable and cost-effective.</p>
                                    </div>
                                </li>
                            </ul>
            </div>
        </div>
    </div>

</section>

<!--
<section id="new-concept" >
    <div class="container" style="padding-top: 30px; padding-bottom: 30px">
        <div class="center">
            <h3  style="color:#34495E">BDKERNEL STORY</h3>
             <p class="lead">Try to break traditional IT business</p>
        </div>  
        <div class="gap"></div>
        <div class="row-fluid">
            <div>
                <p class="text-justify"> In TED TALK, nobel laureate <a href="https://en.wikipedia.org/wiki/Muhammad_Yunus" target="_blank" style="color:#005580">Dr Muhammad Yunus</a>, founding the <a target="_blank" href="https://en.wikipedia.org/wiki/Grameen_Bank" style="color:#005580">Grameen Bank</a> and  pioneering the concepts of microcredit and microfinance, describe the Grameen Bank Methodology amazing way.  It is almost the reverse of conventional Bank Methodology. Like:</p>

                <ul>
                    <li>First Methodology of Grameen banking is that the clients should not go to the bank, it is the bank which should go to the people instead.</li>
                    <li>Conventional banks are owned by the rich, generally men. Grameen Bank is owned by poor women.</li>
                    <li>Objective of the conventional banks is to maximize profit. Grameen Bank's objective is to bring financial services to the poor, particularly women.</li>
                    <li>Conventional banks focus on men, Grameen gives high priority to women. 97 per cent of Grameen Bank's borrowers are women.</li>
                    <li>Grameen Bank branches are located in the rural areas, unlike the branches of conventional banks which try to locate themselves as close as possible to the business districts and urban centers.</li>
                </ul>
                <p class="text-justify">
                    In this video, he inspire the young generation think differently at your work field like he thinked about the Grameen Bank. We got so much inspiration from the first principle of Grameen Bank. We want to establish this concept in IT field. At first we face a problem, a software like ERP, Ecommerce, Business Software and more are need to develop a huge time. Another problem is that this software not need to everyone. To overcome this issue, try to think a software or website that actually everyone need. Only personal website is the things that everyone necessary to make online around the world. We are the first in the world,  We make your website by knocking at your door.
                </p>
                <p>
                    <b>References:</b><br/>
                    <ol>
                        <li><a href="http://muhammadyunus.org/index.php/design-lab/previous-design-labs/43-news-a-media/books-a-articles/232-is-grameen-bank-different-from-conventional-banks">http://muhammadyunus.org/index.php/design-lab/previous-design-labs/43-news-a-media/books-a-articles/232-is-grameen-bank-different-from-conventional-banks</a></li>
                        <li><a href="https://www.youtube.com/watch?v=nYXgfEpgnkk">https://www.youtube.com/watch?v=nYXgfEpgnkk</a></li>
                        <li> <a href="https://www.youtube.com/watch?v=6UCuWxWiMaQ">https://www.youtube.com/watch?v=6UCuWxWiMaQ </a> </li>
                    </ol>
                </p> 
                
            </div>
        </div>
    </div>

</section>
 -->






<!--Footer-->
 <?php include 'layout/footer.php'; ?>
<!--/Footer-->

<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/main.js"></script>

</body>

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:40 GMT -->
</html>
