<?php  
   // require 'client-history.php';  client_history("bdkernel/contact.php")  
?>
<?php 
  session_start();
  if(isset($_POST['submit'])) {
      
      $email_to = "info@bdkernel.com";
      $email_subject = "This message comes from contact page!!!";
  
        $_SESSION["error"] ="";
        $_SESSION["message"] = "";
        $error_message = "";
        
        // validation expected data exists
        if(!isset($_POST['first_name']) ||
            !isset($_POST['last_name']) ||
            !isset($_POST['email']) ||
            !isset($_POST['message'])) {
            $error_message .='We are sorry, but there appears to be a problem with the form you submitted.<br />';     
        }

        $first_name = $_POST['first_name']; // required
        $last_name = $_POST['last_name']; // required
        $email_from = $_POST['email']; // required
        $message = $_POST['message']; // required
 
        
        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

        if(!preg_match($email_exp,$email_from)) {
          $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
        }
       
          $string_exp = "/^[A-Za-z .'-]+$/";
       
        if(!preg_match($string_exp,$first_name)) {
          $error_message .= 'The First Name you entered does not appear to be valid.<br />';
        }
       
        if(!preg_match($string_exp,$last_name)) {
          $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
        }
       
        if(strlen($message) < 2) {
          $error_message .= 'The Comments you entered do not appear to be valid.<br />';
        }
       
        if(strlen($error_message) > 0) {
             $_SESSION["error"] = $error_message;
        }
        else{
            $email_message = "Form details below.\n\n";

            function clean_string($string) {
              $bad = array("content-type","bcc:","to:","cc:","href");
              return str_replace($bad,"",$string);
            }
    
            $email_message .= "First Name: ".clean_string($first_name)."\n";
            $email_message .= "Last Name: ".clean_string($last_name)."\n";
            $email_message .= "Email: ".clean_string($email_from)."\n";
            $email_message .= "Message: ".clean_string($message)."\n";
    
            $headers = 'From: '.$email_from."\r\n".
            'Reply-To: '.$email_from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
            
            mail($email_to, $email_subject, $email_message);   
            $_SESSION["message"] = "Thank you for contacting us. We will be in touch with you very soon.";
            //echo  $_SESSION["message"];
        }
        
        header("Location: contact.php");
        exit;
  }
?>

<!DOCTYPE html>
<head>
     <?php include 'layout/cssJsResource.php'; ?>

     <style type="text/css">
       
       .innerStepList li {
            box-sizing: border-box;
            display: inline-block;
            padding: 10px;
            vertical-align: top;
            width: 100%;
        }
     </style>


      <script type="text/javascript">
          $(document).ready(function () {
             $(".mainNav li").removeClass("active");
             $("#contactNavId").addClass("active");
          });
      </script>

</head>

<body>

    <!--Header-->
     <?php include 'layout/header.php'; ?>
    <!-- /header -->

    <section id="bannerArea">
        <div class="bannerBlock" style="background-image: url(images/sample/slider/personal-website-banner.jpg); ">
            <div class="container">
                <div class="row">
                    <div class="bannnerOneFourthBlock">
                        <div class="servicesContent">
                            <h4 class="bannerTitle">CONTACT</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 



    <section class="innerStepSection">
        <div class="container" style="padding: 20px;">
            <div class="servicesContent">
                <h3 class="sectionTitle">Contact</h3>
            </div>
            <div class="contact">
                <div class="span8">
                      <p>
                        <?php 
                            if(isset($_SESSION["message"]) &&  $_SESSION["message"] != "" ){
                              echo "<span style='color:green'> <b>".$_SESSION["message"]."</b></span>"; 
                              session_unset(); 
                            }
                            else if( isset($_SESSION["error"]) &&  $_SESSION["error"] != "" ){
                              echo "<span style='color:red'><b>".$_SESSION["error"]."</b></span>"; 
                              session_unset(); 
                            }
                        ?>
                      </p>
                      <form   method="post" action="contact.php">
                        <div class="row-fluid">
                          <div class="span5">
                              <label>First Name</label>
                              <input type="text" name="first_name" class="input-block-level" required="required" placeholder="Your First Name">
                              <label>Last Name</label>
                              <input type="text" name="last_name" class="input-block-level" required="required" placeholder="Your Last Name">
                              <label>Email Address</label>
                              <input type="text" name="email" class="input-block-level" required="required" placeholder="Your email address">
                          </div>
                          <div class="span7">
                              <label>Message</label>
                              <textarea name="message" id="message" required="required" class="input-block-level" rows="8"></textarea>
                          </div>

                      </div>
                      <input type="submit" name="submit" class="btn btn-primary btn-large pull-right" value="Send Message" />
                      <p> </p>

                  </form>
              </div>

                <div class="span3">
                    <h4>Our Address</h4>
                      <ul class="unstyled address">
                    <li>
                        <i class="icon-home"></i><strong>Address:</strong> House:21/B (1<SUP>st</SUP> floor), <br/>Nikunja-02, Khilkhet<br>Dhaka-1229<br>Bangladesh
                    </li>
                    <li>
                        <i class="icon-envelope"></i>
                        <strong>Email: </strong> info@bdkernel.com
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <strong>Website:</strong> www.bdkernel.com
                    </li>
                    <!-- <li>
                        <i class="icon-phone"></i>
                        <strong>Cell No:</strong> +88 01612 151 215 
                    </li>
                    <li>
                        <i class="icon-phone"></i>
                        <strong>Cell No:</strong> +88 01722 091 707 
                    </li> -->
                </ul>
                </div>
            </div>
        </div>
    </section>


 <?php include 'layout/footer.php'; ?>
<!--/Footer-->


</body>

<!-- Mirrored from timeknit.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Feb 2017 03:28:40 GMT -->
</html>
